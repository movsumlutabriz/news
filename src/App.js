import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

window.ee = new window.EventEmitter();
console.log(window.ee);

var my_news = [
  {
    author: 'Саша Печкин',
    text: 'В четверг,...',
    bigText: 'четвертого числа...'
  },
  {
    author: 'Просто Вася',
    text: 'Считаю, что $ должен стоить 35 рублей!',
    bigText: 'стоить 35 рублей!'
  },
  {
    author: 'Василий',
    text: 'ЧМ по футболу уже скоро!',
    bigText: 'стоить 35 рублей!'
  }
];

function onChangeHandler(event){
   this.setState({value: event.target.value})
 }

class Add extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      btnIsDisabled: true,
    }
  }

  componentDidMount(){
    ReactDOM.findDOMNode(this.refs.author).focus();
  }

  onButtonClick(){
    var textEl = ReactDOM.findDOMNode(this.refs.text);
    var author = ReactDOM.findDOMNode(this.refs.author).value;
    var text = ReactDOM.findDOMNode(this.refs.text).value;

    var item = [{
      author: author,
      text: text,
      bigText: '...'
    }];

    window.ee.emit('News.add', item);

    textEl.value = '';
    this.setState({btnIsDisabled: true});
  }

  preventForm(e){
    e.preventDefault();
  }

  inputChange(){
    if(ReactDOM.findDOMNode(this.refs.author).value.trim() && ReactDOM.findDOMNode(this.refs.text).value.trim() && ReactDOM.findDOMNode(this.refs.checkrule).checked){
      this.setState({btnIsDisabled: false});
    } else {
      this.setState({btnIsDisabled: true});
    }
  }

  render(){
    return(
      <form
        onSubmit={this.preventForm}
        className="add">
        <div>
          <input
            onChange={this.inputChange.bind(this)}
            type="text"
            className="add_author"
            defaultValue=''
            placeholder="Your Name"
            ref="author"
          />
        </div>
        <div>
          <textarea
            onChange={this.inputChange.bind(this)}
            className='add__text'
            defaultValue=''
            placeholder='Текст новости'
            ref='text'
          ></textarea>
        </div>
        <div>
          <label className="add__checkrule">
            <input type="checkbox" ref='checkrule' onChange={this.inputChange.bind(this)}/>Я согласен с правилами
          </label>
        </div>
        <div>
          <button
            className='add__btn'
            onClick={this.onButtonClick.bind(this)}
            ref='alert_button'
            disabled={this.state.btnIsDisabled}>
            добавить новость!
          </button>
        </div>
      </form>
    );
  }
}

class Article extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      visible: false
    }
  }

  readmoreClick(event){
    event.preventDefault();
    if(this.state.visible == true){
      this.setState({visible: false})
    }else{
      this.setState({visible: true})
    }
  }

  render(){
    var author = this.props.author;
    var text = this.props.text;
    var bigText = this.props.bigText;
    var visible = this.state.visible;

    return(
      <div className="article">
        <p className="article__author">{author}</p>
        <p className="article__text">{text}</p>
        <p
          className={"article__bigtext " + (visible ? '':'hidden__state')}
          onClick={this.readmoreClick.bind(this)}>{bigText}
        </p>
        <a href="#"
          className={"article__readmore " + (visible ? 'hidden__state':'')}
          onClick={this.readmoreClick.bind(this)}>подробнее
        </a>
      </div>
    );
  }
}

class News extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      counter : 0,
      news: props.data
    }
  }

  componentDidMount(){
    var self = this;
    window.ee.on('News.add', function(item){
      var nextNews = item.concat(self.state.news);
      self.setState({news: nextNews});
    })
  }

  componentWillUnmount(){
    window.ee.removeListener('News.add');
  }

  onTotalClock(){
    this.setState({
      counter: ++this.state.counter
    })
  }

  render(){
    var data = this.state.news;
    if(data.length > 0){
      return(
        <div>
          {data.map(function(item, index){
            return (
                <div key={index}>
                  <Article author={item.author} text={item.text} bigText={item.bigText} />
                </div>
            )
          })}
          <strong>
            всего новостей: {data.length}
          </strong>
          </div>
       )
     } else {
        return(
          <div>
          <p>новостей нет!</p>
          </div>
        );
     }
  }
}

News.propTypes = {
  data: PropTypes.array.isRequired
};

Article.propTypes = {
  author: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  bigText: PropTypes.string.isRequired
}

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      news: my_news
    }
  }

  render(){
    return (
      <div>
      <Add />
      <News data={my_news}/>
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

export default App;
